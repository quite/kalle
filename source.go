package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/studio-b12/gowebdav"
)

type Source interface {
	ListFiles() ([]string, error)
	Open(name string) (io.Reader, error)
	Close(name string) error
}

type sourceWebDav struct {
	listed     bool
	calDavRoot string
	calDavPath string
	client     *gowebdav.Client
}

type sourceFS struct {
	listed bool
	fsPath string
	opened map[string]*os.File
}

func NewSource(s *Settings) Source {
	if s.CalDav.FSPath == "" {
		// TODO this is too hardcoded
		calDavPath := fmt.Sprintf("/calendars/%s/%s", s.CalDav.User, s.CalDav.Calendar)
		client := gowebdav.NewClient(s.CalDav.Root, s.CalDav.User, s.CalDav.Password)
		return &sourceWebDav{
			listed:     false,
			calDavRoot: s.CalDav.Root,
			calDavPath: calDavPath,
			client:     client,
		}
	}

	fmt.Fprintln(os.Stderr, "CalDav source is filesystem")
	return &sourceFS{
		listed: false,
		fsPath: s.CalDav.FSPath,
		opened: make(map[string]*os.File),
	}
}

func (s *sourceWebDav) ListFiles() ([]string, error) {
	if err := s.client.Connect(); err != nil {
		s.client = nil
		return nil, fmt.Errorf("Failed to connect to caldav server: %s", s.calDavRoot)
	}

	fileinfos, err := s.client.ReadDir(s.calDavPath)
	if err != nil {
		return nil, fmt.Errorf("Failed to read calendar directory, giving up!")
	}

	var files []string
	for _, fi := range fileinfos {
		files = append(files, fi.Name())
	}

	s.listed = true
	return files, err
}

func (s *sourceWebDav) Open(name string) (io.Reader, error) {
	if !s.listed {
		return nil, fmt.Errorf("Open() without preceding ListFiles()")
	}

	reader, err := s.client.ReadStream(fmt.Sprintf("%s/%s", s.calDavPath, name))

	return reader, err
}

func (s *sourceWebDav) Close(name string) error {
	return nil
}

func (s *sourceFS) ListFiles() ([]string, error) {
	if len(s.opened) > 0 {
		return nil, fmt.Errorf("sourceFS refused new Files() because some file open")
	}

	fileinfos, err := ioutil.ReadDir(s.fsPath)
	if err != nil {
		return nil, fmt.Errorf("Failed to read calendar directory, giving up!")
	}

	var files []string
	for _, fi := range fileinfos {
		name := fi.Name()
		if !fi.IsDir() && strings.HasSuffix(name, ".ics") {
			files = append(files, name)
		}
	}

	s.listed = true
	return files, err
}

func (s *sourceFS) Open(name string) (io.Reader, error) {
	if !s.listed {
		return nil, fmt.Errorf("Open() without preceding ListFiles()")
	}

	_, ok := s.opened[name]
	if ok {
		return nil, fmt.Errorf("file `%s` already opened", name)
	}
	f, err := os.Open(fmt.Sprintf("%s/%s", s.fsPath, name))
	if err != nil {
		return nil, err
	}

	s.opened[name] = f
	return f, nil
}

func (s *sourceFS) Close(name string) error {
	f, ok := s.opened[name]
	if !ok {
		return fmt.Errorf("file `%s` is not opened", name)
	}

	return f.Close()
}
