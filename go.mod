module bitbucket.org/gmelchett/kalle

go 1.12

require (
	bitbucket.org/gmelchett/textcolor v0.0.0-20190402082142-15ef984cbd66
	github.com/arran4/golang-ical v0.0.0-20190329225906-b063ac95b037
	github.com/davecgh/go-spew v1.1.1
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/peterh/liner v1.1.0
	github.com/studio-b12/gowebdav v0.0.0-20190103184047-38f79aeaf1ac
	github.com/teambition/rrule-go v1.3.0
	golang.org/x/sys v0.0.0-20190402054613-e4093980e83e // indirect
)
