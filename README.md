# WIP Kalle - a caldav console calender WIP

NOTE: This pre-alpha software. Nothing is really working as it is supposed to do.


# Current status
* Day view shows summary and location
* Week and eight day view with one character as 30 minutes

# TODO
* Week and eight day view lacks summary and location
* All day events missing
* Holidays - https://github.com/rickar/cal
* Save calender for offline usage
* Design command line arguments. Like "kalle add wed 15:00-16:00 'Dentist'"
* Offline handing of commands
* Next five events missing repeated events
* Month view
* Year view
* Terminal width adjustments?
* Obfuscated password storage?
* Decoration isn't that nice (yet)
* Is https://github.com/laurent22/ical-go/ better than github.com/arran4/golang-ical ?? ical-go has duration, neither has RRULE.

# Requirements
* A terminal with 256 colors and unicode blocks

# FAQ:
* Why this weird home-made locale?
* It looks like this because I always use en_US, but when it comes to the calender I want it in Swedish, with Swedish holidays etc
